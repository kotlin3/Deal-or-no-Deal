package com.bufkin.deal_or_no_deal

import java.lang.Math.floor
import java.lang.Math.sqrt


internal class BankerOffer(private val remainingCases: List<IconButton?>?) {
  private val casesLeft: Int = remainingCases!!.size
  private var offerValue: Double
  private var multiplication: Double
  fun calculateOffer(): Double {
    var offer = 0.0
    for (remainingCase in remainingCases!!) {
      if (remainingCase != null) {
        offer += remainingCase.getValue()
      }
    }
    offer /= casesLeft.toDouble()
    offer = sqrt(offer)
    offer = roundToHundreds(offer)
    offerValue = offer
    return offer
  }

  private fun roundToHundreds(value_: Double): Double {
    var value = value_
    value *= multiplication
    value = floor(value)
    value /= 100
    value = roundToInt().toDouble()
    value *= 100
    multiplication += 0.25
    return value
  }

  init {
    offerValue = 0.0
    multiplication = 0.25
  }
}