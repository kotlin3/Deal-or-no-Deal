package com.bufkin.deal_or_no_deal

import java.awt.Color
import java.awt.Dimension
import java.io.Serial
import java.util.*
import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JPanel

class RemainingValuesPanel : JPanel() {
    private val labelArray = Array(20) {
        ValueButton(0)
        ValueButton(1)
        ValueButton(2)
        ValueButton(3)
        ValueButton(4)
        ValueButton(5)
        ValueButton(6)
        ValueButton(7)
        ValueButton(8)
        ValueButton(9)
        ValueButton(10)
        ValueButton(11)
        ValueButton(12)
        ValueButton(13)
        ValueButton(14)
        ValueButton(15)
        ValueButton(16)
        ValueButton(17)
        ValueButton(18)
        ValueButton(19)
    }

    private val panel: JPanel = JPanel()
    private val buttonArray: MutableList<ValueButton>
    var pressed: Boolean = false

    private val dim = Dimension(30, 35)

    fun deactivateValueButton(value: Int) {
        buttonArray[ValueButton.values.indexOf(value)].background = Color.GRAY
    }

    private fun addLabel(label: ValueButton) {
        label.alignmentX
        panel.add(label)
        panel.add(Box.createRigidArea(dim))
    }

    fun createPanel(): JPanel {

        panel.layout = BoxLayout(panel, BoxLayout.PAGE_AXIS)

        for ((counter, item) in labelArray.withIndex()) addLabel(item)

        return panel
    }

    companion object {
        @Serial
        private val serialVersionUID = -4545323445301422353L
    }

    init {
        buttonArray = ArrayList()

        for (item in labelArray) {
            buttonArray.add(item)
        }
    }
}