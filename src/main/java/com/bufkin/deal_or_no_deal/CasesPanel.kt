package com.bufkin.deal_or_no_deal

import com.bufkin.deal_or_no_deal.*
import java.awt.GridLayout
import java.io.Serial
import java.util.*
import javax.swing.JPanel

class CasesPanel(ui: UI, rvpLeft: RemainingValuesPanel, rvpRight: RemainingValuesPanel) : JPanel() {
    private val panel: JPanel = JPanel()

    private var buttonArray =
            Array(20) {
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
                IconButton(ui, rvpLeft, rvpRight, this)
            }

    companion object {
        @Serial private val serialVersionUID = -2449830916003593421L
    }

    init {
        val layout = GridLayout(5, 4, 15, 15)
        panel.layout = layout
        val buttons: MutableList<IconButton> = ArrayList()

        for (item in buttonArray) {
            buttons.add(item)
        }
    }
}
