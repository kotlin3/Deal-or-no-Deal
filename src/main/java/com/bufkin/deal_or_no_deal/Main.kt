package com.bufkin.deal_or_no_deal

import javax.swing.UIManager

internal object Main {
  var casesToOpen = 0

  fun main(args: Array<String>) {
    try {
      for (info in UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus" == info.name) {
          UIManager.setLookAndFeel(info.className)
          break
        }
      }
    } catch (e: Exception) {
      System.err.print("LookAndFeel kann nicht gefunden werden.")
    }
    val ui = UI()
    val frame = ui.createUI(ui)
    val sleepTime = 100

    // ABLAUF
    // eigenen Koffer wählen
    ui.text = "Bitte wähle einen Koffer."
    while (!IconButton.firstFlag!!) {
      Thread.sleep(sleepTime.toLong())
    }

    // 7 Koffer öffnen
    casesToOpen = 7
    ui.text = "Bitte $casesToOpen Koffer öffnen."
    while (IconButton.openedCases != 8) {
      Thread.sleep(sleepTime.toLong())
    }

    // Bankerangebot
    var offer = BankerOffer(IconButton.remainingCases)
    ui.showOfferWindow(frame, offer.calculateOffer())

    // 6 Koffer öffnen
    casesToOpen = 6
    ui.text = "Bitte $casesToOpen Koffer öffnen."
    while (IconButton.openedCases != 14) {
      Thread.sleep(sleepTime.toLong())
    }

    // Bankerangebot
    offer = BankerOffer(IconButton.remainingCases)
    ui.showOfferWindow(frame, offer.calculateOffer())

    // 5 Koffer öffnen
    casesToOpen = 5
    ui.text = "Bitte $casesToOpen Koffer öffnen."
    while (IconButton.openedCases != 19) {
      Thread.sleep(sleepTime.toLong())
    }

    // Bankerangebot
    offer = BankerOffer(IconButton.remainingCases)
    ui.showOfferWindow(frame, offer.calculateOffer())

    // gewählten Koffer behalten oder anderen Koffer wählen
    ui.text = "Möchten Sie ihren Koffer behalten oder den anderen verbleibenden öffnen?"
  }
}
