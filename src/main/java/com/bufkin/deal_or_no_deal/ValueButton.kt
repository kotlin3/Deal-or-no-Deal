package com.bufkin.deal_or_no_deal

import java.awt.Color
import java.awt.Font
import java.io.Serial
import java.util.*
import javax.swing.JButton

class ValueButton(index: Int) : JButton() {
  private var name: String
  private var text: String
  private var value: Int
  private var index: Int

  private fun fillAvailableValueList() {
    for (i in values) {
      val s = String.format("%,d", i)
      possibleValues.add("$s €")
    }
  }

  override fun getName(): String {
    return this.name
  }

  override fun setName(name: String) {
    this.name = name
  }

  override fun getText(): String {
    return this.text
  }

  override fun setText(text: String) {
    this.text = text
  }

  companion object {
    @Serial private val serialVersionUID = -5799597332533519813L
    private val possibleValues: MutableList<String> = ArrayList()

    var values: List<Int>
        get() = ArrayList(
            listOf(
                1,
                2,
                5,
                10,
                20,
                50,
                100,
                250,
                500,
                750,
                1000,
                2000,
                5000,
                10000,
                20000,
                25000,
                50000,
                100000,
                150000,
                250000
            )
        )
        set(value) = TODO()
  }

  init {
    fillAvailableValueList()
    this.index = index
    this.text = possibleValues[index]
    value = values[index]
    this.name = "label$index"
    background = Color.YELLOW
    setText(this.text)
    font = Font("Arial", Font.BOLD, 15)
    setName(this.name)
    this.isVisible = true
  }
}
