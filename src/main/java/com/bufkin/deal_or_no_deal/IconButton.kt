package com.bufkin.deal_or_no_deal

import java.awt.Color
import java.awt.Font
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.io.Serial
import java.text.NumberFormat
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import javax.swing.ImageIcon
import javax.swing.JButton

class IconButton(ui: UI?, rvpLeft: RemainingValuesPanel, rvpRight: RemainingValuesPanel, casesPanel: CasesPanel?) :
  JButton(), ActionListener {
  private val rvpLeft: RemainingValuesPanel
  private val rvpRight: RemainingValuesPanel
  private var casesPanel: CasesPanel? = null
  private var value: Int
  private var number: Int
  private var pressed: Boolean
  private lateinit var remainingCases: Array<IconButton>

  override fun actionPerformed(e: ActionEvent) {
    if (!pressed) {
      openedCases++
      remainingCases.run { drop(indexOf(e.source)) }

      if (!firstFlag!!) {
        setFirstCase()
        firstFlag = true
      } else {
        this@IconButton.icon = ImageIcon("images/briefcase3.png")
        background = Color.GRAY
        text = displayText
        rvpLeft.deactivateValueButton(value)
        rvpRight.deactivateValueButton(value)
        super.repaint()
        Main.casesToOpen--
        text = "Bitte " + Main.casesToOpen + " Koffer öffnen."
      }
    }
  }

  private fun setFirstCase() {
    foreground = Color.BLACK
    this.icon = ImageIcon("images/briefcase_gold.png")
    firstCase = this
  }

  private val randomValue: Int
    get() {
      val possibleValues: MutableList<Int> = availableValues
      possibleValues.removeAll(usedValues)
      val rand = ThreadLocalRandom.current().nextInt(0, possibleValues.size)
      usedValues.add(possibleValues[rand])
      return possibleValues[rand]
    }

  private fun generateName(): String {
    return "button$value"
  }

  private val displayText: String
    get() {
      var text = NumberFormat.getNumberInstance(Locale.GERMAN).format(value.toLong())
      return " €".let { text += it; text!! }
    }


  @JvmName("getValue1")
  fun getValue(): Int {
    return this.value
  }

  companion object {
    @Serial
    private val serialVersionUID = -3761057170156106141L

    @Volatile
    var firstFlag: Boolean? = null

    @Volatile
    private var firstCase: IconButton? = null

    @Volatile
    var openedCases = 0

    @Volatile
    var remainingCases: MutableList<IconButton?>? = null
    private val usedValues: MutableList<Int> = ArrayList()
    private val availableValues: MutableList<Int> = LinkedList(
      listOf(
        1, 2, 5, 10, 20, 50, 100,
        250, 500, 750, 1000, 2000, 5000, 10000, 20000, 25000, 50000, 100000, 150000, 250000
      )
    )

    @Volatile
    private var caseNumber = 1
  }

  init {
    ui.also { this.ui = it }
    this.rvpLeft = rvpLeft
    this.rvpRight = rvpRight
    this.casesPanel = casesPanel
    value = randomValue
    this.text = displayText
    font = Font("Arial", Font.BOLD, 30)
    val image = ImageIcon("images/briefcase2.png")
    pressed = java.lang.Boolean.FALSE
    this.name = generateName()
    number = caseNumber
    caseNumber++
    text = number.toString()
    this.icon = image
    addActionListener(this)
    openedCases = 0
    firstFlag = java.lang.Boolean.FALSE
    this.isEnabled = java.lang.Boolean.TRUE
    horizontalTextPosition = CENTER
    verticalTextPosition = CENTER
  }
}