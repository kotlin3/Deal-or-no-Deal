package com.bufkin.deal_or_no_deal

import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.event.WindowEvent
import javax.swing.*

class UI : JComponent() {
    private val middleTextField: JTextField = JTextField("")
    private val frame: JFrame = JFrame()
    var text: String?
        get() = middleTextField.text
        set(input) {
            middleTextField.text = input
            middleTextField.repaint()
            middleTextField.validate()
        }

    fun createUI(ui: UI?): JFrame {
        val panelLeft = RemainingValuesPanel()
        val panelRight = RemainingValuesPanel()
        val panelCenter = ui?.let { CasesPanel(it, panelLeft, panelRight) }
        val dim = Dimension(1000, 800)
        frame.layout = BorderLayout()
        frame.size = dim
        frame.preferredSize = dim
        val left = panelLeft.createPanel()
        val right = panelRight.createPanel()
        val center = panelCenter.createPanel()
        val mid = JPanel()
        mid.layout = BorderLayout()
        middleTextField.isEditable = false
        mid.add(middleTextField, BorderLayout.PAGE_START)
        mid.add(center, BorderLayout.CENTER)
        frame.contentPane.add(left, BorderLayout.WEST)
        frame.contentPane.add(right, BorderLayout.EAST)
        frame.contentPane.add(mid, BorderLayout.CENTER)
        frame.isVisible = true
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        return frame
    }

    fun showOfferWindow(frame: JFrame?, offer: Double) {
        val s = String.format("%,d", offer.roundToInt())
        val options = arrayOf<Any>("Deal", "No Deal")
        val n =
                JOptionPane.showOptionDialog(
                        frame,
                        "Wollen Sie das Angebot von $s € annehmen?",
                        "Bankerangebot",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[0]
                )
        if (n == 0) {
            JOptionPane.showMessageDialog(frame, "Herzlichen Glückwunsch zu $s €!")
            frame!!.dispatchEvent(WindowEvent(frame, WindowEvent.WINDOW_CLOSING))
        }
    }
}
